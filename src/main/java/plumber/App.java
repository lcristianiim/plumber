package plumber;

import model.Pipe;
import service.PipeGenerator;

import java.util.ArrayList;
import java.util.Arrays;

import static service.Combination.combination;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        int numberOfGeneratedPipes = 35; // MAX VALUE CAN BE 35
        int desiredLength = 200;

        PipeGenerator storage = new PipeGenerator(numberOfGeneratedPipes);
//        System.out.println(storage.getPipeList().size());

        displayPipes(storage);
        System.out.println("=====================================================================================================================");
        System.out.println("Customer desired lenght: " + desiredLength);
        System.out.println("=====================================================================================================================");

//        Scanner sc = new Scanner(System.in);
//        System.out.println("WELCOME THE THE PLUMBER APP");
//        System.out.println("Please enter the length for your project in cm (for 1 meter, type 100");
//
//        int number = sc.nextInt();

        Object[] elements = new Object[] {};

        for (int i = 0; i < storage.getPipeList().size(); i++) {

            elements = appendValue(elements, storage.getPipeList().get(i).getLength());
        }
        System.out.println("=====================================================================================================================");
        System.out.println("PROPOSED COMBINATIONS: ");
        System.out.println("----------------------");
        combination(elements, 5, desiredLength);
//        for (int i = 2; i < numberOfGeneratedPipes; i++) {
//            combination(elements, i, desiredLength);
//      }
        System.out.println("=====================================================================================================================");
    }

    private static Object[] appendValue(Object[] obj, Object newObj) {

        ArrayList<Object> temp = new ArrayList<Object>(Arrays.asList(obj));
        temp.add(newObj);
        return temp.toArray();

    }

    private static void displayPipes(PipeGenerator storage) {
        System.out.println("=====================================================================================================================");
        System.out.println("The store containes the following pipes:");
        for (Pipe pipe : storage.getPipeList()) {
             System.out.print(pipe.getLength() + " ");
        }
        System.out.println();
        System.out.println("=====================================================================================================================");

    }
}