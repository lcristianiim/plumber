package service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import model.Pipe;

import java.util.*;

/**
 * Created by cristian on 14.11.2016.
 */
@Getter
@Setter
@NoArgsConstructor
public class PipeGenerator  {
    private List<Pipe> pipeList;

    public PipeGenerator(int numberOfPipes) {
        List<Integer> availableTypes = generateAvailableTypes();

        List<Pipe> pipes = createRandomPipesList(numberOfPipes, availableTypes);
        pipes.sort(Comparator.comparing(Pipe::getLength));


        pipeList = pipes;

    }

    private List<Pipe> createRandomPipesList(int numberOfPipes, List<Integer> availableTypes) {
        List<Pipe> pipes = new ArrayList<Pipe>();
        addPipesToList(numberOfPipes, availableTypes, pipes);
        return pipes;
    }

    private void addPipesToList(int numberOfPipes, List<Integer> availableTypes, List<Pipe> pipes) {
        for (int i = 0; i < numberOfPipes; i++) {
            Pipe pipe = new Pipe(getRandomType(availableTypes));
            pipes.add(pipe);
        }
    }

    private int getRandomType(List<Integer> availableTypes) {
        Random random = new Random();
        int index = random.nextInt(availableTypes.size());
        int type = availableTypes.get(index);
        return type;
    }

    private List<Integer> generateAvailableTypes() {
        List<Integer> availableTypes = new ArrayList<Integer>();

        availableTypes.add(10);
        availableTypes.add(15);
        availableTypes.add(20);
        availableTypes.add(25);
        availableTypes.add(30);
        availableTypes.add(45);
        availableTypes.add(55);
        availableTypes.add(60);
        availableTypes.add(75);
        availableTypes.add(90);
        availableTypes.add(100);
        availableTypes.add(130);
        availableTypes.add(145);
        availableTypes.add(150);

        return availableTypes;
    }
}