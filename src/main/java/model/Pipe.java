package model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by cristian on 14.11.2016.
 */
@Getter
@Setter
public class Pipe {
    private int length;

    public Pipe(int length) {
        this.length = length;
    }

}
